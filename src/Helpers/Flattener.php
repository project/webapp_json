<?php

namespace Drupal\webapp\Helpers;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;

/**
 * Flatten fields.
 */
class Flattener implements CacheableDependencyInterface {

  protected $flatKey = 'extended';

  public function __construct() {
    if (array_key_exists('flat', $_GET)) {
      $this->flatKey = 'flat';
    }
  }

  public function flatten($nestedArray) {
    $flatNestedArray = [];
    foreach ($nestedArray as $key => $item) {
      $flatNestedArray[$key] = $item;
      if (is_array($item) && count($item) === 1) {
        $singleItem = reset($item);
        if (array_key_exists('value', $singleItem)) {
          $flatNestedArray[$key] = $singleItem['value'];
          if (array_key_exists('url', $singleItem)) {
            $flatNestedArray['url'] = $singleItem['url'];
          }
        }
        if (array_key_exists('target_id', $singleItem)) {
          $flatNestedArray[$key] = $singleItem['target_id'];
        }
      }
    }

    return $flatNestedArray;
  }

  /**
   * The cache contexts associated with this object.
   *
   * These identify a specific variation/representation of the object.
   *
   * Cache contexts are tokens: placeholders that are converted to cache keys by
   * the @cache_contexts_manager service. The replacement value depends on the
   * request context (the current URL, language, and so on). They're converted
   * before storing an object in cache.
   *
   * @return string[]
   *   An array of cache context tokens, used to generate a cache ID.
   *
   * @see \Drupal\Core\Cache\Context\CacheContextsManager::convertTokensToKeys()
   */
  public function getCacheContexts() {
    return ['url'];
  }

  /**
   * The cache tags associated with this object.
   *
   * When this object is modified, these cache tags will be invalidated.
   *
   * @return string[]
   *   A set of cache tags.
   */
  public function getCacheTags() {
    return [$this->flatKey];
  }

  /**
   * The maximum age for which this object may be cached.
   *
   * @return int
   *   The maximum time in seconds that this object may be cached.
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }
}
