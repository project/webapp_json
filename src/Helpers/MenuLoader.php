<?php

namespace Drupal\webapp\Helpers;

use Drupal\file\Entity\File;

/**
 * Get menu content.
 */
class MenuLoader {

  const EXCLUDE_MENUS = [
    'admin' => 'admin',
    'tools' => 'tools',
    'account' => 'account'
  ];

  public function getMenuTree() {
    $menuTree = [];
    $menuList = menu_ui_get_menus();

    $menuList = array_diff_key($menuList, self::EXCLUDE_MENUS);

    foreach ($menuList as $menuKey => $menuName) {
      $menuTree[$menuKey] = $this->getMenuData($menuKey);
    }

    return $menuTree;
  }

  private function getMenuData($menuKey) {
    $menu_tree = \Drupal::menuTree();

    // Build the typical default set of menu tree parameters.
    $parameters = $menu_tree->getCurrentRouteMenuTreeParameters($menuKey);

    // Load the tree based on this set of parameters.
    $tree = $menu_tree->load($menuKey, $parameters);

    $renderedMenuTree = $this->parseMenuTree($tree);

    return $renderedMenuTree;
  }

  private function parseMenuTree($tree) {
    $menuTree = [];

    foreach ($tree as $treeItem) {
      if (!$treeItem->link->isEnabled()) {
        continue;
      }

      unset($menuItem);

      $treeItemLink = $treeItem->link;
      $urlObj = $treeItemLink->getUrlObject();

      if ($urlObj->isRouted() || $urlObj->isExternal()) {
        $uuid = $treeItemLink->getDerivativeId();

        if ($uuid) {
          $entity = \Drupal::service('entity.repository')
            ->loadEntityByUuid('menu_link_content', $uuid);
          if ($entity->hasField('thumbnail')) {
            $thumbNail = $entity->get('thumbnail');
            $thumbNailValue = $thumbNail->getValue()[0];
            $imageFile = File::load($thumbNailValue['target_id']);
            if ($imageFile) {
              $menuItem['thumbnail_url'] = file_url_transform_relative($imageFile->Url());
            }
          }
          if ($entity->hasField('icon')) {
            $menuItem['icon_id'] = $entity->get('icon')->value;
          }
        }
      }

      $treeItemLink = $treeItem->link;
      $treeItemUrl = $treeItemLink->getUrlObject();

      $menuItem['title'] = $treeItemLink->getTitle();

      $menuItem = array_merge($treeItem->options, $menuItem);

      if ($treeItemUrl->isExternal()) {
        $menuItem['is_external'] = true;
        $menuItem['uri'] = $treeItemUrl->getUri();
      } else {
        $menuItem['is_external'] = false;
        $menuItem['internal_path'] = '/' . $treeItemUrl->getInternalPath();
        $menuItem['alias'] = \Drupal::service('path.alias_manager')->getAliasByPath($menuItem['internal_path']);
      }

      if ($treeItem->hasChildren) {
        $menuItem['children'] = $this->parseMenuTree($treeItem->subtree);
      }

      $menuTree[] = $menuItem;
    }

    return $menuTree;
  }

}
