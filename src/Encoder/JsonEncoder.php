<?php

namespace Drupal\webapp\Encoder;

use Drupal\serialization\Encoder\JsonEncoder as SerializationJsonEncoder;

/**
 * Encodes WEBAPP data in JSON.
 *
 * Simply respond to webapp_json format requests using the JSON encoder.
 */
class JsonEncoder extends SerializationJsonEncoder {

  /**
   * The formats that this Encoder supports.
   *
   * @var string
   */
  protected static $format = ['webapp_json'];

}
